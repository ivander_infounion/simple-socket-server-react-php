<?php

/**
 * See bootstrap/app to configure events
 */

/**
 * One access point for all events
 * {site}:{port}/connect
 */
$router->get('connect', 'SocketController@connect');

if (env('DEBUG', 0) > 0) {
    include_once 'test.php';
}


if (!empty(config('messageEvents'))) {
    foreach (config() as $className => $data) {
        app()->bind($className, $data['class']);
    }
}