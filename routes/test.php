<?php
config([
    'messageEvents' => array_merge(config()->get('messageEvents', []), [
        'simpleMessage' => [ // name of event
            // message class
            'class' => App\Sockets\Messages\SimpleMessage::class,
        ],
        'listenerById' => [ // name of event
            // message class
            'class' => App\Sockets\Messages\ModelChangeMessage::class,
            // optional, relation class, RELATION_PEER_TO_PEER as default
            'relation' => App\Sockets\Message::RELATION_CLIENT_SERVER,
            // serverKey is key to insure that message sends from server, not from client
            // put in in Token header
            'serverKey' => env('DEFAULT_SERVER_KEY'),
        ],
        'addQueueMessage' => [ // name of event
            // message class
            'class' => App\Sockets\Messages\AddQueueMessage::class,
            // optional, relation class, RELATION_PEER_TO_PEER as default
            'relation' => App\Sockets\Message::RELATION_CLIENT_SERVER,
            // serverKey is key to insure that message sends from server, not from client
            // put in in Token header
            'serverKey' => env('DEFAULT_SERVER_KEY'),
        ],
        'serveQueueMessage' => [ // name of event
            // message class
            'class' => App\Sockets\Messages\ServeQueueMessage::class,
        ],
    ]),
]);

$router->get('/', function () use ($router) {
    return "
            <script>
                var conn = new WebSocket('ws://" . env('APP_SOCKET_ADDRESS',
            "localhost") . "/connect');
                conn.onopen = function(e) {
                    console.log(\"Connection established!\");
                };
                conn.onmessage = function(e) {
                    console.log('Socket sends data:' + e.data);
                };
            </script>
        ";
});

// TESTS FOR QUEUE

$sendmessagesCount = 100;
$parameters['sendMessageEveryNanosec'] = 1000;

for ($i = 0; $i < $sendmessagesCount; $i++) {
    $messages[] = [
        'id' => $i,
        'text' => 'Sms message text for message ' . $i,
        'phone' => '+380666666661',
    ];
}

$parameters['serverSendsEventCommand'] = [
    'event' => 'addQueueMessage',
    'token' => env('DEFAULT_SERVER_KEY'),
    'data' => $messages,
];
$parameters['clientSubscribeEvent'] = [
    'event' => 'serveQueueMessage',
    'queue' => 'Ukraine',
    'type' => 'listen',
];
$parameters['clientReportEvent'] = [
    'event' => 'serveQueueMessage',
    'queue' => 'Ukraine',
    'data' => [
        "isSuccess" => true,
        "messageData" => '',
        "customerInfo" => [
            "phone" => "customer info from client (phone)",
            "device-id" => "customer info from client (device-id)",
            "sim-type" => 1,
        ],
        "sendTime" => 12415125,
        "reportTime" => 123657457,
    ],
];

$router->get('/queue/server', function () use ($router, $parameters) {
    return "
            <script>
                var conn = new WebSocket('ws://" . env('APP_SOCKET_ADDRESS',
            "localhost") . "/connect');
                conn.onopen = function(e) {
                    console.log('Connection ok, sending initial messages');
                    conn.send('" . json_encode($parameters['serverSendsEventCommand']) . "');
                };
                conn.onmessage = function(e) {
                    console.log('Socket sends data:' + e.data);
                };
            </script>
        ";
});

$router->get('/queue/client', function () use ($router, $parameters) {
    return "
            <script>
                var conn = new WebSocket('ws://" . env('APP_SOCKET_ADDRESS',
            "localhost") . "/connect');
                conn.onopen = function(e) {
                    console.log('Connection ok, sending listen command');
                    conn.send('" . json_encode($parameters['clientSubscribeEvent']) . "');
                };
                conn.onmessage = function(e) {
                    console.log('Socket sends data:' + e.data + ' after 3 sec report will be send');
                    var reportBody = JSON.parse('" . json_encode($parameters['clientReportEvent']) . "');
                    reportBody.data.messageData = JSON.parse(e.data);
                    reportBody.data.isSuccess =  true;
//                    reportBody.data.isSuccess =  Math.random() >= 0.5;
                    reportBody.data.sendTime =  Math.floor(Math.random() * 100000);
                    reportBody.data.reportTime = reportBody.data.sendTime + 3000;
                    console.log(reportBody);
                    readyData = JSON.stringify(reportBody);
                    setTimeout(function() {
                        conn.send(readyData);
                    }, " . $parameters['sendMessageEveryNanosec'] . ");
                };
            </script>
        ";
});

$parameters['clientListenCommand'] = [
    'event' => 'simpleMessage',
    'type' => 'listen',
];
$parameters['clientEventCommand'] = [
    'event' => 'simpleMessage',
    'data' => [
        'text' => "All clients except sender receive this message",
    ],
];
$router->get('/simple/client', function () use ($router, $parameters) {
    return "
            <script>
                var conn = new WebSocket('ws://" . env('APP_SOCKET_ADDRESS',
            "localhost") . "/connect');
                conn.onopen = function(e) {
                    console.log('Connection ok, sending listen command message an agter 3 sec message');
                    conn.send('" . json_encode($parameters['clientListenCommand']) . "');
                     setTimeout(function() {
                        conn.send('" . json_encode($parameters['clientEventCommand']) . "');
                    }, 3000);
                };
                conn.onmessage = function(e) {
                    console.log('Socket sends data:' + e.data + ' send random data in 3 seconds');
                    setTimeout(function() {
                        conn.send('" . json_encode($parameters['clientEventCommand']) . "');
                    }, 3000);
                };
                
                
            </script>
        ";
});

$parameters['clientListenCommand'] = [
    'event' => 'listenerById',
    'type' => 'listen',
    'data' => [
        'id' => [
            1,
            3,
            7,
            9,
        ],
    ],
];
$parameters['clientEventCommand'] = [
    'event' => 'listenerById',
    'token' => env('DEFAULT_SERVER_KEY'),
    'data' => [
        'id' => 2,
        'text' => "Model 2 has changed, this message appears only for some subscribers, that interested in",
    ],
];


$router->get('/byid/client', function () use ($router, $parameters) {
    return "
            <script>
                var conn = new WebSocket('ws://" . env('APP_SOCKET_ADDRESS',
            "localhost") . "/connect');
                conn.onopen = function(e) {
                    console.log('Connection ok, sending listen command');
                    conn.send('" . json_encode($parameters['clientListenCommand']) . "');
                };
                conn.onmessage = function(e) {
                    console.log('Socket sends data:' + e.data);
                };
            </script>
        ";
});


$router->get('/byid/server', function () use ($router, $parameters) {
    return "
            <script>
                var conn = new WebSocket('ws://" . env('APP_SOCKET_ADDRESS',
            "localhost") . "/connect');
                conn.onopen = function(e) {
                    console.log('Connection ok, data every 2 seconds');
                     setInterval(function() {
                        var data = JSON.parse('" . json_encode($parameters['clientEventCommand']) . "');
                        data.data.id = Math.floor(Math.random() * 10);
                        console.log('Sending with id ' + data.data.id);
                        conn.send(JSON.stringify(data));
                    }, 2000);
                };
                conn.onmessage = function(e) {
                    console.log('Socket sends data:' + e.data);
                };
                
                
            </script>
        ";
});