<?php
/**
 * Example file of configs by messages events, copy it without underscore
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @created    13.03.19
 */


config([
    'messageEvents' => [
        'simpleMessage' => [ // name of event
            // message class
            'class' => App\Sockets\Messages\SimpleMessage::class,
        ],
        'listenerById' => [ // name of event
            // message class
            'class' => App\Sockets\Messages\ModelChangeMessage::class,
            // optional, relation class, RELATION_PEER_TO_PEER as default
            'relation' => App\Sockets\Message::RELATION_CLIENT_SERVER,
            // serverKey is key to insure that message sends from server, not from client
            // put in in Token header
            'serverKey' => env('DEFAULT_SERVER_KEY'),
        ],
        'addQueueMessage' => [ // name of event
            // message class
            'class' => App\Sockets\Messages\AddQueueMessage::class,
            // optional, relation class, RELATION_PEER_TO_PEER as default
            'relation' => App\Sockets\Message::RELATION_CLIENT_SERVER,
            // serverKey is key to insure that message sends from server, not from client
            // put in in Token header
            'serverKey' => env('DEFAULT_SERVER_KEY'),
        ],
        'serveQueueMessage' => [ // name of event
            // message class
            'class' => App\Sockets\Messages\ServeQueueMessage::class,
        ],
    ],
]);