<?php
/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    26.02.19
 */

namespace App\Queue;


use Ratchet\ConnectionInterface;

class QueueBranch
{
    /**
     * @var string name of queue branch
     */
    public $name;

    /**
     * @var \SplObjectStorage of QueueMessageItem`s
     */
    private $_waiting;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->_waiting = app(AbstractQueueBranchRepository::class);
    }

    /**
     * Return next message ready for send
     * @return QueueMessageItem|null
     */
    public function getMessageToSend(): ?QueueMessageItem
    {
        if (!empty($this->_waiting)) {
            foreach ($this->_waiting as $message) {
                $this->_waiting->detach($message);
                /** @var QueueMessageItem $message */
                return $message;
            }
        }
        return null;
    }

    /**
     * Put new message to sending queue
     * @param QueueMessageItem $message
     */
    public function addNewMessage(QueueMessageItem $message)
    {
        $this->_waiting->attach($message);
    }

    public function deleteMessageIfConnectionMatch(ConnectionInterface $connection)
    {
        foreach ($this->_waiting as $waitingItem) {
            /** @var QueueMessageItem $waitingItem */
            if ($waitingItem->hasConnectionDependencies($connection)){
                $this->_waiting->detach($waitingItem);
            }
        }
    }

}