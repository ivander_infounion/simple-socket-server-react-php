<?php
/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    26.02.19
 */

namespace App\Queue;


use App\Sockets\MessageInterface;
use Ratchet\ConnectionInterface;

class QueueMessageItem
{

    const DATA_FIELD_HASH = 'hash';

    private $_data;

    /**
     * @var \Ratchet\ConnectionInterface connection to message source client
     */
    private $_initializerConnection;

    /**
     * @var \Ratchet\ConnectionInterface connection to recipient that process operation
     */
    private $_recipientConnection;


    public function onConnectionLostWith(ConnectionInterface $connection)
    {
        if ($this->hasConnectionDependencies($connection)) {
            $queue = app(Queue::class);
            switch ($connection) {
                case $this->_recipientConnection:
                    // move to waiting if in process
                    $queue->moveToWaitingIfInProcessing($this);
                    break;
                case $this->_initializerConnection:
                    $queue->deleteFromAllQueues($this);
                    break;
                default:
                    break;
            }
        }
    }

    public function hasConnectionDependencies(ConnectionInterface $connection): bool
    {
        return $this->_initializerConnection === $connection || $this->_recipientConnection === $connection;
    }


    public function __construct(array $data, MessageInterface $message)
    {
        $this->_data = $data;
        $this->_initializerConnection = $message->getSenderConnection();
        $this->_initializerConnection = $message->getSenderConnection();
    }

    public function reportToInitializer(array $data)
    {
        $this->_initializerConnection->send(json_encode($data));
    }

    public function getClientDataArray()
    {
        return $this->_data;
    }

    public function getClientData()
    {
        return json_encode($this->getClientDataArray());
    }

    /**
     * @return ConnectionInterface
     */
    public function getInitializerConnection(): ConnectionInterface
    {
        return $this->_initializerConnection;
    }

}