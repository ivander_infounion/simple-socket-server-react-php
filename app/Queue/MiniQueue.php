<?php
/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    26.02.19
 */

namespace App\Queue;

use Ratchet\ConnectionInterface;

class MiniQueue extends Queue
{

    /**
     * @var array[QueueBranch]
     */
    public $queues;

    public function getQueue(string $queueId): ?QueueBranch
    {
        return $this->queues[$queueId] ?? null;
    }

    public function removeQueueById(string $queueId): void
    {
        if (isset($this->queues[$queueId])) {
            unset($this->queues[$queueId]);
        };
    }

    /**
     * @param string|null $queueId
     *
     * @return QueueBranch
     */
    public function getOrCreateQueue(?string $queueId): QueueBranch
    {
        if (!isset($this->queues[$queueId])) {
            $this->queues[$queueId] = $this->createNewQueue($queueId);
        }
        return $this->queues[$queueId];
    }

    /**
     * @param string $queueId
     *
     * @return QueueBranch
     */
    public function createNewQueue(string $queueId): QueueBranch
    {
        return new QueueBranch($queueId);
    }

    public function deleteFromQueues(ConnectionInterface $connection): void
    {
        if (!empty($this->queues)) {
            foreach ($this->queues as $queue) {
                $queue->deleteMessageIfConnectionMatch($connection);
            }
        }
    }

}