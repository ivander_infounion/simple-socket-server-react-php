<?php
/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    26.02.19
 */

namespace App\Queue;


use Ratchet\ConnectionInterface;

abstract class Queue
{
    const QUEUE_NAME_FIELD = 'queue';

    const CONNECTION_CLOSE_INITIALIZER_REPORT = [
        'status' => false,
        'message' => 'Lost connection with recipient, adding item back to queue',
    ];


    /**
     * @var \SplObjectStorage
     */
    private $_processing;

    public function __construct()
    {
        $this->_processing = new \SplObjectStorage();
    }

    /**
     * Get next message to send from queue
     *
     * @param $queueId
     *
     * @return QueueMessageItem|null
     */
    public function getMessage($queueId): ?QueueMessageItem
    {
        if (($queue = $this->getQueue($queueId)) !== null && ($message = $queue->getMessageToSend()) !== null) {
            $this->_processing->attach($message);
        }
        return $message ?? null;
    }

    /**
     * Get message from already processing by comparing full message data
     * TODO test when client sends wrong data fields
     *
     * @param array $data
     *
     * @return QueueMessageItem|null
     */
    public function getProcessingByData(array $data): ?QueueMessageItem
    {
        /** @var QueueMessageItem $processingItem */
        foreach ($this->_processing as $processingItem) {
            if ($processingItem->getClientDataArray() == $data) {
                return $processingItem;
            }
        }
        return null;
    }

    /**
     * Process message operations on different actions
     *
     * @param QueueMessageItem $message
     * @param array $failureData data that initializer receive
     * @param bool $returnToWaitingQueue will message returns to queued items that waiting to process
     *
     * @return string|null queue id if message added to queue again
     */
    public static function processMessageAction(
        QueueMessageItem $message,
        array $failureData,
        bool $returnToWaitingQueue = false
    ) {
        $queue = app(Queue::class);
        $message->reportToInitializer($failureData);
        $queueId = $queue->deleteFromProcessing($message);
        if (!empty($queueId) && $returnToWaitingQueue) {
            $queue->addNewMessage($message, $queueId);
        }
        return $queueId ?? null;
    }

    public function moveToWaitingIfInProcessing(QueueMessageItem $messageItem)
    {
        if ($this->_processing->contains($messageItem)) {
            self::processMessageAction(
                $messageItem,
                self::CONNECTION_CLOSE_INITIALIZER_REPORT,
                true
            );
        }
    }

    /**
     * Delete message from processing and return his queue name, if exists
     *
     * @param QueueMessageItem $item
     *
     * @return string|null
     */
    public function deleteFromProcessing(QueueMessageItem $item): ?string
    {
        $info = null;
        if ($this->_processing->contains($item)) {
            $info = $this->_processing->offsetGet($item);
            $this->_processing->detach($item);
        }
        return $info[self::QUEUE_NAME_FIELD] ?? null;
    }

    public function deleteFromAllQueues(QueueMessageItem $messageItem): void
    {
        $this->deleteFromProcessing($messageItem);
        $this->deleteFromQueues($messageItem->getInitializerConnection());
    }

    /**
     * Add new message to queue by queue id
     *
     * @param QueueMessageItem $message
     * @param $queueId
     */
    public function addNewMessage(QueueMessageItem $message, ?string $queueId): void
    {
        $queue = $this->getOrCreateQueue($queueId);
        $queue->addNewMessage($message);
    }

    /**
     * Get next message from queue by id
     *
     * @param string|null $queueId
     *
     * @return QueueMessageItem|null
     */
    public function getMessageToSend(?string $queueId): ?QueueMessageItem
    {
        $message = null;
        $queue = $this->getQueue($queueId);
        if ($queue !== null) {
            if (($message = $queue->getMessageToSend()) !== null) {
                $this->_processing->attach($message, [self::QUEUE_NAME_FIELD => $queueId]);
            } else {
                $this->removeQueueById($queueId);
            }
        }
        return $message;
    }

    public function onConnectionClose(ConnectionInterface $connection)
    {
        foreach ($this->_processing as $processingItem) {
            /** @var QueueMessageItem $processingItem */
            $processingItem->onConnectionLostWith($connection);
        }
        $this->deleteFromQueues($connection);
    }

    abstract function getQueue(string $queueId): ?QueueBranch;
    abstract function removeQueueById(string $queueId): void ;

    /**
     * @param string|null $queueId
     *
     * @return QueueBranch
     */
    abstract function getOrCreateQueue(?string $queueId): QueueBranch;

    /**
     * @param string $queueId
     *
     * @return QueueBranch
     */
    abstract function createNewQueue(string $queueId): QueueBranch;


    /**
     * Delete message from all queues
     *
     * @param QueueMessageItem $messageItem
     */
    abstract function deleteFromQueues(ConnectionInterface $connection): void;


}