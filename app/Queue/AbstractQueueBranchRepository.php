<?php
/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    27.02.19
 */

namespace App\Queue;


use ArrayAccess;
use Countable;
use Iterator;
use Serializable;

abstract class AbstractQueueBranchRepository implements  Countable, Iterator, Serializable, ArrayAccess
{

}