<?php
/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    25.02.19
 */

namespace App\Console\Commands;

use App\Sockets\SimpleSocket as SynadynSocketAlias;
use Illuminate\Console\Command;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\MessageComponentInterface;
use Ratchet\WebSocket\WsServer;


class RunServerCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "server:run";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Listen to incoming web sockets connections to process data";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $socket = new SynadynSocketAlias();
            $wsServer = new WsServer($socket);
            $server = IoServer::factory(
                new HttpServer($wsServer),
                (int) env('APP_SOCKET_PORT', 8080)
            );

            $wsServer->enableKeepAlive($server->loop, 30);

            $server->run();
        } catch (\Exception $e) {
            $this->error("An error occurred during socket server connection");
        }
    }

}