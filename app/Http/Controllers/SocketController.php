<?php

namespace App\Http\Controllers;

use App\Sockets\SimpleSocket;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Laravel\Lumen\Routing\Controller as BaseController;

class SocketController extends BaseController
{
    public function connect()
    {
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new SimpleSocket()
                )
            ),
            (int) env('APP_SOCKET_PORT', 8080)
        );

        $server->run();
    }

}
