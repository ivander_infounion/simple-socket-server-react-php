<?php
/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    26.02.19
 */

namespace App\Providers;

use App\Queue\AbstractQueueBranchRepository;
use App\Queue\MiniQueue;
use App\Queue\Queue;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class QueueServiceProvider extends ServiceProvider
{

    /**
     * Register queue manager.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Queue::class, function ($app) {
            return new MiniQueue();
        });

        $this->app->bind(AbstractQueueBranchRepository::class, function ($app, $params) {
            return new \SplObjectStorage;
        });
    }

}