<?php
/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    26.02.19
 */

namespace App\Providers;

use App\Queue\Queue;
use App\Sockets\AbstractClientRepository;
use App\Sockets\ClientRepository;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class ClientRepositoryProvider extends ServiceProvider
{

    /**
     * Register queue manager.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(AbstractClientRepository::class, function () {
            return new ClientRepository();
        });
    }

}