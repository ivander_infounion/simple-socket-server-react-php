<?php
/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    27.02.19
 */

namespace App\Sockets;


use Ratchet\ConnectionInterface;

abstract class AbstractClientRepository implements \SplSubject
{

    abstract function newClient(ConnectionInterface $connection);

    abstract function processMessage(MessageInterface $message);

    abstract function removeClient(ConnectionInterface $connection);

    abstract function attach(\SplObserver $observer);

    abstract function countActiveClients(): int;

    abstract function detach(\SplObserver $observer);

}