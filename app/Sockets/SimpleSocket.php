<?php
/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    25.02.19
 */

namespace App\Sockets;


use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

class SimpleSocket implements MessageComponentInterface
{
    protected $_clients;

    public function __construct()
    {
        $this->_clients = app(AbstractClientRepository::class);
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->_clients->newClient($conn);
        debugMessage("Client added, active clients: " . $this->_clients->countActiveClients() . " \n");
    }

    public function onMessage(ConnectionInterface $senderConnection, $msg)
    {
        debugMessage("New message received \n");
        $this->_clients->processMessage(MessageFactory::create($senderConnection, $msg));
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->_clients->removeClient($conn);
        debugMessage("Client close connection, active clients: " . $this->_clients->countActiveClients() . " \n");
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        debugMessage("An error has occurred: {$e->getMessage()} {$e->getFile()} {$e->getLine()}, closing connection\n");
        $this->_clients->removeClient($conn);
    }

}