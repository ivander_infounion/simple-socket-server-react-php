<?php
/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    25.02.19
 */

namespace App\Sockets;


use App\Queue\Queue;
use Ratchet\ConnectionInterface;
use SplSubject;

class ClientRepository implements SplSubject
{
    /**
     * @var MessageInterface
     */
    private $_message;

    public function newClient(ConnectionInterface $connection)
    {
        $client = new Client($connection);
        $this->attach($client);
    }

    public function processMessage(MessageInterface $message)
    {
        $this->setMessage($message);
        if ($this->getMessage()->isListenCommand()) {
            $this->getUserByConnection($message->getSenderConnection())->update($this);
        } elseif ($this->getMessage()->canProcessItself()) {
            $message->processNewData();
        } elseif ($this->getMessage()->isAllowedToPost()) {
            $this->notify();
        }
    }

    public function notify()
    {
        /** @var \SplObserver $observer */
        foreach ($this->_clients as $client) {
            $client->update($this);
        }
    }

    public function removeClient(ConnectionInterface $connection)
    {
        app(Queue::class)->onConnectionClose($connection);
        if (($user = $this->getUserByConnection($connection)) !== null) {
            $this->detach($user);
            $connection->close();
        }
    }

    /**
     * @param ConnectionInterface $connection
     *
     * @return Client
     */
    private function getUserByConnection(ConnectionInterface $connection): ?Client
    {
        foreach ($this->_clients as $client) {
            if ($client->hasConnection($connection)) {
                /** @var Client $client */
                return $client;
            }
        }
        return null;
    }


    /**
     * @var \SplObjectStorage
     */
    private $_clients;

    public function __construct()
    {
        $this->_clients = new \SplObjectStorage();
    }

    public function attach(\SplObserver $observer)
    {
        $this->_clients->attach($observer);
    }

    public function countActiveClients(): int
    {
        return count($this->_clients);
    }

    public function detach(\SplObserver $observer)
    {
        $this->_clients->detach($observer);
    }

    /**
     * @return MessageInterface
     */
    public function getMessage(): MessageInterface
    {
        return $this->_message;
    }

    /**
     * @param MessageInterface $message
     */
    private function setMessage(MessageInterface $message): void
    {
        $this->_message = $message;
    }

}