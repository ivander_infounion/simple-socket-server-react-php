<?php
/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    25.02.19
 */

namespace App\Sockets;

use Ratchet\ConnectionInterface;
use SplObserver;
use SplSubject;

class Client implements SplObserver
{

    private $_connection;
    private $_events = [];


    public function __construct(ConnectionInterface $connection)
    {
        $this->_connection = $connection;
    }

    public function update(SplSubject $subject)
    {
        /** @var ClientRepository $subject */
        if (
            $subject->getMessage()->isListenCommand()
            && $this->hasConnection($subject->getMessage()->getSenderConnection())
        ) {
            if (!$this->isListenEvent($subject->getMessage())) {
                debugMessage("Subscription request received \n");
                $this->subscribeEvent($subject->getMessage());
                if (!empty($response = $subject->getMessage()->processSubscription())) {
                    $this->_connection->send($response);
                }
            }
        } elseif (
            $this->isListenEvent($subject->getMessage())
            && !$this->hasConnection($subject->getMessage()->getSenderConnection())
        ) {
            debugMessage("Subscriber found for message, sending data \n");
            if (!empty($response = $subject->getMessage()->processNewData())) {
                $this->_connection->send($response);
            }
        }
    }


    public function hasConnection(ConnectionInterface $connection)
    {
        return $this->_connection === $connection;
    }


    public function isListenEvent(MessageInterface $message)
    {
        return array_key_exists($message->getEvent(), $this->_events)
            && $this->eventDataSubscriptionMatch($message);
    }

    public function subscribeEvent(MessageInterface $message)
    {
        $this->_events[$message->getEvent()][] = $message->getChecker();
    }

    private function eventDataSubscriptionMatch(MessageInterface $message)
    {
        foreach ($this->_events[$message->getEvent()] as $checker) {
            /** @var callable $checker is closure to ensure that client subscription with parameters matches message */
            if ($checker($message) === true) {
                return true;
            }
        }
        return false;
    }
}