<?php
/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    25.02.19
 */

namespace App\Sockets;


use Ratchet\ConnectionInterface;

abstract class MessageFactory
{

    /**
     * @param ConnectionInterface $connection
     * @param string $message
     *
     * @return mixed
     * @throws \Exception
     */
    public static function create(ConnectionInterface $connection, string $message = '')
    {
        $data = self::decodeJson($message);
        return self::createMessageCommand($connection, $data);
    }

    public static function getMessageEvents($eventName = null)
    {
        $events = config('messageEvents');
        return $eventName !== null ? $events[$eventName] ?? null : $events;
    }

    /**
     * @param $message
     *
     * @return array
     * @throws \Exception
     */
    private static function decodeJson($message): array
    {
        $message = json_decode($message, true);
        if (json_last_error() !== JSON_ERROR_NONE || empty($message)) {
            throw new \Exception("Json code is empty or invalid. \n");
        }
        return $message;
    }

    private static function createMessageCommand(ConnectionInterface $connection, array $data = [])
    {
        if (!isset($data['event'])) {
            throw new \InvalidArgumentException('Input message should contain "event" field');
        } elseif (($messageEventConfig = self::getMessageEvents($data['event'])) === null) {
            throw new \InvalidArgumentException('Message event not found');
        }
        $className = $messageEventConfig['class'];
        unset($messageEventConfig['class']);
        try {
            $message = app()->makeWith($className, [
                'inputData' => $data,
                'senderConnection' => $connection,
                'config' => $messageEventConfig,
            ]);
        } catch (\Error $e) {
            throw new \InvalidArgumentException('Wrong class configuration');
        }
        return $message;
    }
}