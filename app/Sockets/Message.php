<?php
/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    25.02.19
 */

namespace App\Sockets;


use Ratchet\ConnectionInterface;
use Ratchet\WebSocket\WsConnection;

abstract class Message implements MessageInterface
{
    /**
     * Fields in message body structure
     */
    const MESSAGE_FIELD_EVENT = 'event';
    const MESSAGE_FIELD_TYPE = 'type';
    const MESSAGE_FIELD_TOKEN = 'token';
    const MESSAGE_FIELD_DATA = 'data';

    /**
     * Values of commands
     */
    const COMMAND_TYPE_LISTEN = 'listen';

    /**
     * Relation types for this message type
     */
    const RELATION_PEER_TO_PEER = 0; // all client can send messages
    const RELATION_CLIENT_SERVER = 1; // only authorized by server key client can send messages to chanel

    /**
     * @var int relation between listeners and message sources
     */
    public $relation = self::RELATION_PEER_TO_PEER;

    /**
     * @var string|null server key to insure that sender allowed to post messages, leave null to allow all
     * clients to post messages
     *
     * Works only with combination of relation === RELATION_CLIENT_SERVER
     */
    public $serverKey;

    /**
     * @var array
     */
    private $_inputData;

    /**
     * @var WsConnection
     */
    private $_senderConnection;

    public function __construct(array $inputData, ConnectionInterface $senderConnection, $config = [])
    {
        $this->setInputData($inputData);
        $this->setSenderConnection($senderConnection);
        $this->configure($config);
    }

    private function configure(array $config = [])
    {
        foreach ($config as $attributeName => $value) {
            if (property_exists($this, $attributeName)) {
                $this->$attributeName = $value;
            }
        }
    }

    private function getSenderToken()
    {
        return $this->getInputData()[self::MESSAGE_FIELD_TOKEN] ?? null;
    }

    public function isAllowedToPost(): bool
    {

        return $this->relation === self::RELATION_PEER_TO_PEER
            || ($this->relation === self::RELATION_CLIENT_SERVER && $this->serverKey == $this->getSenderToken());
    }

    /**
     * @return ConnectionInterface
     */
    public function getSenderConnection(): ConnectionInterface
    {
        return $this->_senderConnection;
    }

    /**
     * @param ConnectionInterface $senderConnection
     */
    private function setSenderConnection(ConnectionInterface $senderConnection): void
    {
        $this->_senderConnection = $senderConnection;
    }

    public function isListenCommand(): bool
    {
        return isset($this->getInputData()[self::MESSAGE_FIELD_TYPE])
            && $this->getInputData()[self::MESSAGE_FIELD_TYPE] === self::COMMAND_TYPE_LISTEN;
    }

    public function getEvent(): string
    {
        return $this->getInputData()[self::MESSAGE_FIELD_EVENT] ?? 'default';
    }

    /**
     * @return array
     */
    private function getInputData(): array
    {
        return $this->_inputData;
    }

    /**
     * @return array
     */
    public function getMessageData(): array
    {
        return $this->_inputData[self::MESSAGE_FIELD_DATA] ?? [];
    }

    /**
     * @param array $inputData
     */
    private function setInputData(array $inputData): void
    {
        $this->_inputData = $inputData;
    }

    public function getClientMessage(): string
    {
        return json_encode($this->getInputData()[self::MESSAGE_FIELD_DATA]);
    }

    public function processSubscription(): string
    {
        return "";
    }

    public function processNewData(): string
    {
        return $this->getClientMessage();
    }

    public function canProcessItself(): bool
    {
        return false;
    }
}