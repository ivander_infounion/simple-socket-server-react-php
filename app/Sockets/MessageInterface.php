<?php
/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    25.02.19
 */

namespace App\Sockets;


use Ratchet\ConnectionInterface;

interface MessageInterface
{
    public function __construct(
        array $inputData,
        ConnectionInterface $senderConnection
    ); // receive array of input json data

    public function getSenderConnection(): ConnectionInterface; // return WsConnection of sender

    public function isListenCommand(): bool; // check whether message command is subscribe at event

    public function getEvent(): string; // return event name of message

    public function getClientMessage(): string; // return prepared json for client

    public function getMessageData(): array; // return array of inputted data

    public function getChecker(
    ): callable; // return callable checker, that returns true if message subscription and data matches

    public function isAllowedToPost(): bool; // is this message allowed to post from this source

    public function processSubscription(): string; // triggers when new sub income

    public function processNewData(): string; // triggers when post data income

    public function canProcessItself(): bool; // check whether message dont needs to interact with listeners
}