<?php

namespace App\Sockets\Messages;

use App\Sockets\Message;
use App\Sockets\MessageInterface;

/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    25.02.19
 */
class SimpleMessage extends Message implements MessageInterface
{
    public function getChecker(): callable
    {
        return function () {
            return true;
        };
    }
}