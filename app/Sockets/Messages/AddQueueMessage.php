<?php

namespace App\Sockets\Messages;

use App\Queue\MiniQueue;
use App\Queue\Queue;
use App\Queue\QueueMessageItem;
use App\Sockets\Message;
use App\Sockets\MessageInterface;

/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    25.02.19
 */
class AddQueueMessage extends Message implements MessageInterface
{

    const DATA_FIELD_ID = 'id';
    const DATA_FIELD_QUEUE = 'queue';

    const SUCCESS_ADDING_RESPONSE = ['status' => 1, 'message' => 'All messages added to queue'];

    public function getChecker(): callable
    {
        return function (){
            return true;
        };
    }

    public function canProcessItself(): bool
    {
        return true;
    }

    public function processNewData(): string
    {
        $queueId = $this->getMessageData()[self::DATA_FIELD_QUEUE] ?? 'default';
        /** @var MiniQueue $queue */
        $queue = app(Queue::class);
        $messages = $this->getMessageData();
        foreach ($messages as $messageData) {
            $queue->addNewMessage(new QueueMessageItem($messageData, $this), $queueId);
        }
        $this->getSenderConnection()->send(json_encode(self::SUCCESS_ADDING_RESPONSE));
        return "";
    }

}