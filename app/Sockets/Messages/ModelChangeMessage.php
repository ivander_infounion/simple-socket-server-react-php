<?php

namespace App\Sockets\Messages;

use App\Sockets\Message;
use App\Sockets\MessageInterface;

/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    25.02.19
 */
class ModelChangeMessage extends Message implements MessageInterface
{

    const DATA_FIELD_ID = 'id';

    public function getChecker(): callable
    {
        $ids = $this->extractArrayIds();
        return function (ModelChangeMessage $message) use ($ids) {
            return empty($ids) || count(array_intersect($ids, $message->extractArrayIds())) > 0;
        };
    }

    private function extractArrayIds(): array
    {
        $id = $this->getMessageData()[self::DATA_FIELD_ID] ?? [];
        if (is_int($id) || is_string($id)) {
            $id = [$id];
        }

        return (array)$id;
    }

}