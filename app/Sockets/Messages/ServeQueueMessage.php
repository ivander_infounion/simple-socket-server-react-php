<?php

namespace App\Sockets\Messages;

use App\Queue\MiniQueue;
use App\Queue\Queue;
use App\Queue\QueueMessageItem;
use App\Sockets\AbstractClientRepository;
use App\Sockets\Message;
use App\Sockets\MessageInterface;
use Ratchet\ConnectionInterface;

/**
 * @author     Ivan Derkach <vakafonic@gmail.com>
 * @see        Nuclino documentation
 * @created    25.02.19
 */
class ServeQueueMessage extends Message implements MessageInterface
{

    const DATA_FIELD_ID = 'id';
    const DATA_FIELD_QUEUE = 'queue';
    const DATA_FIELD_MESSAGE = 'message';


    const DATA_FIELD_REPORT_SUCCESS = 'isSuccess';
    const DATA_FIELD_REPORT_MESSAGE = 'messageData';

    const DEFAULT_QUEUE_NAME = 'default';

    /**
     * Returns next message of subscribed queue item or closes connection
     * @return string
     */
    public function processSubscription(): string
    {
        $queueId = $this->getMessageData()[self::DATA_FIELD_QUEUE] ?? self::DEFAULT_QUEUE_NAME;
        return $this->getNextTextOrClose($queueId);
    }

    /**
     * Returns next message item of queue by queueId or closes connection
     *
     * @param string $queueId
     *
     * @return string
     */
    private function getNextTextOrClose(string $queueId): string
    {
        $messageToSend = $this->getNextQueueMessage($queueId);
        if ($messageToSend === null) {
            app(AbstractClientRepository::class)->removeClient($this->getSenderConnection());
            return "";
        }
        return $messageToSend->getClientData();
    }

    /**
     * Returns next message item of queue by queueId
     *
     * @param string $queueId
     *
     * @return string
     */
    private function getNextQueueMessage($queueId): ?QueueMessageItem
    {
        $queue = app(Queue::class);
        return $queue->getMessageToSend($queueId);
    }

    /**
     * Process report and get next message to send or close connection
     * @return string
     */
    public function processNewData(): string
    {
        if ($this->isReportSuccess()) {
            $result = $this->processSuccessReport();
        } else {
            $result = $this->processFailedReport();
        }

        if (!empty($result)) {
            $this->getSenderConnection()->send($result);
        }

        return "";
    }

    private function processSuccessReport(): string
    {
        /** @var MiniQueue $queue */
        $queue = app(Queue::class);
        $message = $queue->getProcessingByData($this->getReportMessageData());
        if ($message !== null) {
            $queueId = $queue::processMessageAction($message, $this->getMessageData());
        }
        return $this->getNextTextOrClose($queueId ?? '');
    }

    private function processFailedReport(): string
    {
        /** @var MiniQueue $queue */
        $queue = app(Queue::class);
        $message = $queue->getProcessingByData($this->getReportMessageData());
        if ($message !== null) {
            $queue::processMessageAction($message, $this->getMessageData(), true);
        }
        app(AbstractClientRepository::class)->removeClient($this->getSenderConnection());
        return "";
    }

    private function isReportSuccess(): bool
    {
        return (bool)$this->getMessageData()[self::DATA_FIELD_REPORT_SUCCESS] ?? false;
    }

    private function getReportMessageData(): array
    {
        $rawdata = $this->getMessageData()[self::DATA_FIELD_REPORT_MESSAGE];

        if ($rawdata === null) {
            $rawdata = [];
        } elseif(is_string($rawdata)) {
            $rawdata = ['report' => $rawdata];
        }

        return $rawdata;
    }

    public function canProcessItself(): bool
    {
        return true;
    }

    public function getChecker(): callable
    {
        return function () {
            return true;
        };
    }

}