# Simple Socket Server

Socket server is simple and flexible tool to process data share between many clients via websocket in real time.
Its written on simple [Lumen framework](https://lumen.laravel.com/docs/5.7)

### Requirements

 - PHP >= 7.1.3

### Installation

Clone project from repository. Then install the dependencies.

```sh
$ composer install
```

Create new virtual host and direct it to public/index.php 
Nginx example virtual host config:

```
server {
    listen 80;
    charset utf-8;
    server_name socket.loc;
    root "/path/to/socket/public";
    index index.php;
    error_log  /var/log/nginx/socket.loc-error.log error;
        
   
    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }
}

```
Configure your .env file

```sh
APP_URL=http://socket.loc
APP_SOCKET_ADDRESS=socket.loc
APP_SOCKET_PORT=8080
DEFAULT_SERVER_KEY=123456
DEBUG=1
```
Restart Nginx.
Next step is to start listening socket worker

## Listen to port messages

To start listening websocket messages you need to start worker

For debug porpuses you can open new ssh connection, proceed to app directory and run

```
./artisan server:run
```

For production dont forget to remove debug and run command via [Systemd](https://help.ubuntu.ru/wiki/systemd) or [Supervisor](http://supervisord.org/)
Dont forget that this process listens port, so you need to set only one daemon, and restart it if it crushes. Example of supervisor config file:

```
[program:socket-server]
command=/usr/bin/php /path/to/socket/artisian server:run
stdout_logfile=/var/log/socket-server.log
autostart=true
autorestart=true
user=www-data
stopsignal=KILL
numprocs=1
```

## HOW-TO and Most Interesting

 * routes/web.php initializes routing and contains messages events to models map. Start configure messages from here and dont forget to add new events to map. Also this file contains examples, uncomment it and run as written
    * goto index page to create client in web
    * goto /test to see json code that server wants to receive
    * send messages via console command
    * see debug messages at terminal window
    * see results in every console of every clients
* app/console/commands/RunServerCommand contains code that starts server

## App flow

Client establishes connection with server (for example use js code in chrome console)

```js
var conn = new WebSocket('ws://server.local:8080/connect');
conn.onopen = function(e) {
   console.log("Connection established!");
};
conn.onmessage = function(e) {
   console.log('Socket sends data: ' + e.data);
};
```

Then client needs to subscribe on interested event ro receive messages from server

```js
conn.send('{"event":"listenerById","type":"listen","data":{"id":[1,2,3]}}');
```

When other client (use different tab) sends message for interested event ...


```js
conn.send('{"event":"listenerById","token":"4cfdc2e157eefe6facb983b1d557b3a1","data":{"id":2,"text":"Model 2 has changed, this message appears only for some subscribers, that interested in"}}');
```

You will see this message on your console

### **Note that events needed to be configured at routes/web.php**

# Test it by yourself

To enable tests run server with debug value 1

## Mode 1 - Peer to Peer messages exchange

In this mode you have simple message exchange with this logic

* Messages can be send by any client
* You will receive message if you subscribed at event
* When client sends message he does not take it back

**to make new client connection open new tab with url = {domain}/simple/client**



## Mode 2 - Server To Client messages exchange

In this mode you have simple server to client message exchange with this logic

* Messages can be send only by server connections with token
* You will receive message if you subscribed at event
* You can filter interested messages by putting id in subscription message

**to make new server connection open new tab with url = {domain}/byid/server**

**to make new client connection open new tab with url = {domain}/byid/client**


## Mode 2 - Server To Client With Queue and Real time report feedback

In this mode you have server to client message exchange with this logic

* Messages can be send only by server connections with token
* Server connection needed to be online, if it interrupts all messages will be deleted from queue
* Server receive report from clients with data
* Server creates or adds new messages to queue by queueId
* Clients subscribes to queue by id and starts to receive messages right after subscription
* If messages in queue = 0 all client connections with this queue will be closed
* If client sends not success report - message throws back to queue and this client connection will be closed 

**to make new server connection open new tab with url = {domain}/queue/server**

**to make new client connection open new tab with url = {domain}/queue/client**


### To test your own connections open console in home page {domain}
